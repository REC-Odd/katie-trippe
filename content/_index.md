+++
title = "Katie Trippe"
tagline = "Social Justice Warrior. Foodie. Dog Lover."
+++

Highly motivated self-starter with an international education, travel, and career portfolio. Detail oriented with excellent research, writing, and intercommunication skills, with experience in gender, post-conflict justice, and peace building.

Passion for community development, social justice, cultural diversity, current affairs, international travel, and continuous learning.
